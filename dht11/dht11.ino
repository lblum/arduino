#include <DHT11.h>
#include <Arduino_JSON.h>

DHT11 dht11(2);

void setup()
{
    Serial.begin(9600);
}

String getData() {
  JSONVar data;

	int temperature = dht11.readTemperature();

  int humidity = dht11.readHumidity();
    data["temperatura"] = String(temperature);

  if (temperature != DHT11::ERROR_CHECKSUM && temperature != DHT11::ERROR_TIMEOUT &&
      humidity != DHT11::ERROR_CHECKSUM && humidity != DHT11::ERROR_TIMEOUT)
  {
    data["temperatura"] = String(temperature);
    data["humedad"] = String(humidity);
  }
  else
  {
    data["temperatura"] = "-1";
    data["humedad"] = "-1";
  }

  return JSON.stringify(data);
}

void loop()
{
  Serial.print(getData());
  Serial.println();

  delay(1000);
}
