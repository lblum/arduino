servo.min() -> va a la posición inicial
servo.max() -> va a la posición máxima
servo.center() -> se posiciona en el medio
servo.to(g) -> salta a la posición g grados, con 0 <= g <=180
servo.step(g) -> se desplaza g grados con respecto a la posición actual 
servo.sweep() -> barre continuo desde min hasta max ida y vuelta
