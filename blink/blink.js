const { Board, Led} = require('johnny-five');

let board, led;

board = new Board();

board.on('ready' , () =>  {
       
    led = new Led(13);

    led.blink(500);
});  

board.on('error' , function (err) {
    console.log(err);
})