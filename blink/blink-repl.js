const { Board, Led} = require('johnny-five');

let board, led;

board = new Board();

board.on('ready' , () =>  {
       
    led = new Led(13);

    board.repl.inject({led});

});  

board.on('error' , function (err) {
    console.log(err);
})