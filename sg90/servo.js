const {Board, Servo} = require("johnny-five");

board = new Board();

board.on("ready", () => {
  const servo = new Servo(9);


  board.repl.inject({
    servo
  });


});
